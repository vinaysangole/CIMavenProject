package com.csd.demo.db.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.csd.demo.db.DBConnection;

public class DAO {
	private DAO dao;

	public DAO getInstance() {
		if (dao == null) {
			dao = new DAO();
		}
		return dao;
	}

	public String getPassWordByUName(String uname) {
		String pass="";
		try {
			Connection connection = DBConnection.getConnection();
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("Select password from csdlogin where username='" + uname + "'");
			while(rs.next()){
				pass=rs.getString(1);
			}
			connection.close();
			statement.close();
			return pass;
		} catch (Exception e) {
			e.printStackTrace();
			return pass;
		}
	}
}