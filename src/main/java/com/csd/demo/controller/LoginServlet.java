package com.csd.demo.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csd.demo.db.dao.DAO;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/HTML");
		String uname = request.getParameter("uname"); 
		String psw = request.getParameter("psw");
		if(psw!=null && uname!=null){
			DAO dao=new DAO().getInstance();
			String passDB = dao.getPassWordByUName(uname);
			if(psw.equals(passDB)){
				request.getRequestDispatcher("welcome.jsp").forward(request, response);
			}else{
				response.getWriter().write(" UserName Or PassWord Not Correct !!");
				request.getRequestDispatcher("index.html").include(request, response);
			}
		}
	}
}
